<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Pokemons Controller
 *
 * @property \App\Model\Table\PokemonsTable $Pokemons
 * @method \App\Model\Entity\Pokemon[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PokemonsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 30,
        ];

        $pokemons = $this->Pokemons->find('all')->contain(['PokemonStats.Stats', 'PokemonTypes.Types']);
        $pokemons = $this->paginate($pokemons);

        $this->set(compact('pokemons'));
    }

    /**
     * View method
     *
     * @param string|null $id Pokemon id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pokemon = $this->Pokemons->get($id, [
            'contain' => ['PokemonStats.Stats', 'PokemonTypes.Types'],
        ]);

        $this->set(compact('pokemon'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pokemon id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pokemon = $this->Pokemons->get($id);
        if ($this->Pokemons->delete($pokemon)) {
            $this->Flash->success(__('The pokemon has been deleted.'));
        } else {
            $this->Flash->error(__('The pokemon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Dashboard method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function dashboard()
    {
        $avgWeight = $this->Pokemons
                    ->find("all")
                    ->where(['id >=' => 387])
                    ->where(['id <=' => 493]);
        $avgWeight = $avgWeight->select(['avg' => $avgWeight->func()->avg('weight')])
                    ->first()
                    ->toArray();
        $this->set(Array('avgWeight'=> $avgWeight['avg']));

        $fairyPokemonsGeneration1 = $this->Pokemons->find("all")
                                ->join([
                                    'pokemon_types' => [
                                        'table' => 'pokemon_types',
                                        'conditions' => 'pokemons.id = pokemon_types.pokemon_id',
                                    ]]);
        $fairyPokemonsGeneration3 = clone $fairyPokemonsGeneration1;
        $fairyPokemonsGeneration7 = clone $fairyPokemonsGeneration1;
        $fairyPokemonsGeneration1->where(["type_id =" => 10])->where(['pokemons.id <=' => 151]);
        $fairyPokemonsGeneration3->where(["type_id =" => 10])->where(['pokemons.id >=' => 252])->where(['pokemons.id <=' => 386]);
        $fairyPokemonsGeneration7->where(["type_id =" => 10])->where(['pokemons.id >=' => 722])->where(['pokemons.id <=' => 809]);
        $fairyPokemonsGeneration1 = $fairyPokemonsGeneration1->select(['count' => $fairyPokemonsGeneration1->func()->count('pokemons.id')])
                                ->first()
                                ->toArray();
        $fairyPokemonsGeneration3 = $fairyPokemonsGeneration3->select(['count' => $fairyPokemonsGeneration3->func()->count('pokemons.id')])
                                ->first()
                                ->toArray();
        $fairyPokemonsGeneration7 = $fairyPokemonsGeneration7->select(['count' => $fairyPokemonsGeneration7->func()->count('pokemons.id')])
                                ->first()
                                ->toArray();
        $this->set(Array('fairyPokemonsGeneration1'=> $fairyPokemonsGeneration1['count']));
        $this->set(Array('fairyPokemonsGeneration3'=> $fairyPokemonsGeneration3['count']));
        $this->set(Array('fairyPokemonsGeneration7'=> $fairyPokemonsGeneration7['count']));

        $fastestsPokemons = $this->Pokemons->find("all")
                            ->join([
                                'pokemon_stats' => [
                                    'table' => 'pokemon_stats',
                                    'type' => 'inner',
                                    'conditions' => 'pokemons.id = pokemon_stats.pokemon_id',
                                ]])
                            ->select(['name'])
                            ->select(['pokemon_stats.value'])
                            ->where(['stat_id = ' => 6])
                            ->order(['value' => 'DESC'])
                            ->limit(10)
                            ->toArray();
        $this->set(compact('fastestsPokemons'));
    }
}
