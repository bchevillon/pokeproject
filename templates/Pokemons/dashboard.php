<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon[]|\Cake\Collection\CollectionInterface $pokemons
 */
?>
<div class="content">
    <h3><?= __('Dashboard') ?></h3>
    <div class="mb-3 bold">
        <h4 class="text-center text-underline ">Average weight of 4th generation pokemon : <?= $avgWeight ?>hg</h4>
    </div>
    <h4 class="text-center mt-5">Number of Fairy-type Pokemon per generation</h4>
    <table class="table">
        <thead class="thead-dark text-center">
            <tr>
            <th class="text-center" scope="col">Generation</th>
            <th class="text-center" scope="col">Number of Fairy-type Pokemon</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th class="text-center" scope="row">1</th>
            <td class="text-center"><?= $fairyPokemonsGeneration1 ?></td>
            </tr>
            <tr>
            <th class="text-center" scope="row">3</th>
            <td class="text-center"><?= $fairyPokemonsGeneration3 ?></td>
            </tr>
            <tr>
            <th class="text-center" scope="row">7</th>
            <td class="text-center"><?= $fairyPokemonsGeneration7 ?></td>
            </tr>
        </tbody>
    </table>
    <h4 class="text-center mt-5">Fastests pokemons</h4>
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th class="text-center" scope="col">#</th>
            <th class="text-center" scope="col">Pokemon</th>
            <th class="text-center" scope="col">Speed</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($fastestsPokemons as $key=>$pokemon): ?>
                <tr>
                <th class="text-center" scope="row"><?=$key+1 ?></th>
                <td class="text-center"><?= $pokemon['name'] ?></td>
                <td class="text-center"><?= $pokemon['pokemon_stats']['value'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
