<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 */
?>

<body>
    <div class="content">
        <h3><?= __('View') ?></h3>
        <div class="row">
            <div class="col-md-6 row justify-content-center align-self-center row">
                <?= $this->Html->image($pokemon->main_sprite); ?>
            </div>
            <div class="col-md-6">
                <h1 class="card__name" style="font-size:60px;"><?= h($pokemon->name) ?></h1>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <h5 class="card_type card__type <?= $pokemon->first_type ?> card--<?= $pokemon->first_type ?>"><?= $pokemon->first_type ?></h5>
                    </div>
                    <?php if(!empty($pokemon->second_type)) : ?>
                        <div class="col-md-6">
                            <h5 class="card__second_type <?= $pokemon->second_type ?>"><?= $pokemon->second_type ?></h5>                  
                        </div>
                    <?php endif; ?>
                    <div class="table-responsive">
                        <table class="card__stats my-5">
                            <tbody>
                                <?php $combine = array_combine(array("HP","Defense","Attack","Special Attack", "Special Defense","Speed"),$pokemon->pokemon_stats) ?>
                                <?php foreach($combine as $name => $pokemonStats): ?>
                                    <tr>
                                        <td class="text-center text-light card--<?= $pokemon->first_type ?>" scope="row"><?= $name ?></td>
                                        <td class="text-center"><?= $pokemonStats->value ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div id="carouselExampleControls" class="carousel slide w-50 mx-auto" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active text-center">
                        <div><?= $this->Html->image($pokemon->main_sprite); ?>
                        <h2 class="card__name">Front</h2>
                        </div>
                    </div>
                    <div class="carousel-item text-center">
                        <?= $this->Html->image($pokemon->back_sprite); ?>
                        <h2 class="card__name">Back</h2>
                    </div>
                    <div class="carousel-item text-center">
                        <?= $this->Html->image($pokemon->shiny_sprite); ?>
                        <h2 class="card__name">Shiny</h2>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#000000" class="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                        </svg></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#000000" class="bi bi-arrow-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                        </svg></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <!--Scripts for the carousel -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>